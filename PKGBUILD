# Maintainer: Bernhad Landauer <bernhard@manjaro.org>
# Arch Maintainer: Felix Yan <felixonmars@archlinux.org>
# Arch Maintainer: Antonio Rojas <arojas@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgname=okular
pkgver=23.08.2
pkgrel=2
pkgdesc='Document Viewer'
arch=(aarch64 x86_64)
url='https://apps.kde.org/okular/'
license=(GPL LGPL FDL)
groups=(kde-applications kde-graphics)
replaces=(okular-mobile)
provides=(okular-mobile)
conflicts=(okular-mobile)
depends=(djvulibre libspectre libkexiv2 poppler-qt5 kpty5 kactivities5 threadweaver5 kjs kparts5 purpose5 discount phonon-qt5)
makedepends=(extra-cmake-modules ebook-tools kdegraphics-mobipocket kdoctools5 khtml chmlib)
optdepends=('ebook-tools: mobi and epub support'
            'kdegraphics-mobipocket: mobi support' 'libzip: CHM support'
            'khtml: CHM support' 'chmlib: CHM support' 'calligra: ODT and ODP support'
            'unrar: Comic Book Archive support' 'unarchiver: Comic Book Archive support (alternative)'
            'kde-cli-tools: to configure web shortcuts' 'plasma-workspace: to configure web shortcuts')
source=(https://download.kde.org/stable/release-service/$pkgver/src/$pkgname-$pkgver.tar.xz{,.sig})
sha256sums=('25a69e1e666925e52c57d1b09beb72ad3a61a61328daf042359c3f6a740f2edd'
            'SKIP')
validpgpkeys=(CA262C6C83DE4D2FB28A332A3A6A4DB839EAA6D7  # Albert Astals Cid <aacid@kde.org>
              F23275E4BF10AFC1DF6914A6DBD2CE893E2D1C87  # Christoph Feck <cfeck@kde.org>
              D81C0CB38EB725EF6691C385BB463350D6EF31EF) # Heiko Becker <heiko.becker@kde.org>
options=(!zipman)

build() {
  cmake -B build -S $pkgname-$pkgver \
    -DBUILD_TESTING=OFF \
    -DOKULAR_UI="both"
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
}
